package br.senai.sp.informatica.jogosDB.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() {
		try {
			// Registrando o driver jdbc
			Class.forName("com.mysql.jdbc.Driver");
			
			//Retorna uma conex�o com o banco de dados gerada pelo DriveManager
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesDB?autoReconnect=tru&useSSL=false", "root", "root132");
		}catch (SQLException e){
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
}