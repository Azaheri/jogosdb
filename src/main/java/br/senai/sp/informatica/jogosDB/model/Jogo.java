package br.senai.sp.informatica.jogosDB.model;

public class Jogo {

	private Long id;
	private String nome;
	private String desenvolvedora;
	private String categoria;
	private String plataformas;
	
	public Jogo(Long id, String nome, String desenvolvedora, String categoria, String plataformas) {
		super();
		this.id = id;
		this.nome = nome;
		this.desenvolvedora = desenvolvedora;
		this.categoria = categoria;
		this.plataformas = plataformas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDesenvolvedora() {
		return desenvolvedora;
	}

	public void setDesenvolvedora(String desenvolvedora) {
		this.desenvolvedora = desenvolvedora;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getPlataformas() {
		return plataformas;
	}

	public void setPlataformas(String plataformas) {
		this.plataformas = plataformas;
	}
	
	
	
/*
	 * create table jogos(
			id bigint auto_increment primary key,
			nome varchar(255) not null,
			desenvolvedora varchar(255) not null,
			categoria varchar(255) not null,
			plataformas varchar(255) not null
		);
*/	
	
}
